class Box :

	def __init__ (self):
		self._contents = []
		self.etat = "open"
		self.capacity = None

	def add (self, truc):
		if self.etat == "open":
			self._contents.append(truc)
		else:
			print("La boite est fermée")

	def is_in(self, truc):
		return truc in self._contents

	def action_look(self):
		if self.etat == "open":
			print("La boite contient : " + ", ".join( self._contents))
		else:
			print ("La boite est fermée")

	def rmv(self, truc):
		self._contents.remove(truc)

	def is_open(self):
		return self.etat

	def open(self):
		self.etat = "open"

	def close(self):
		self.etat = "close"

	def __contains__ (self, truc):
		return self.is_in(truc)

	def set_capacity(self,cap):
		self.capacity = cap

	def capacity(self):
		return self.capacity

	def has_room_fort(self,t):
		total=0
		for elem in self.__contents :
			total+=elem.getVolume()
		placerestante=self.capacity-total
		if t.getVolume()>placerestante :
			return False
		else :
			return True

class Thing:
	def __init__(self,volume):
		self.volume = volume

	def getVolume(self):
		return self.volume
